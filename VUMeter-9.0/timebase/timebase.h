#pragma once

/** \file **********************************************************************
 *
 * podstawa czasu aplikacji - 1ms
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>

typedef void timebase_handler_ft(void *arg);

/*
 * uruchamia timer w trybie CTC rejestruje funkcję zwrotną i włącza przerwanie
 * TIMER2_COMP_vect, rozdzielczość podstawy czasu: 1ms (1kHz)
 *
 * @handler	funkcja wykonywana w przerwaniu timera
 * @arg		argument funkcji
 */
void  timebase_start(timebase_handler_ft *handler, void *arg) __gcc_nonnull_args(1);

