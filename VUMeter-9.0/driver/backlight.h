#pragma once

/** \file **********************************************************************
 *
 * obsługa podświetlania wskaźników vu
 *
 ******************************************************************************/

#include <stdint.h>

typedef enum {
	BRIGHTNESS_NONE,
	BRIGHTNESS_MINIMUM,
	BRIGHTNESS_LOW,
	BRIGHTNESS_MEDIUM,
	BRIGHTNESS_HIGH,
	BRIGHTNESS_MAXIMUM
} brightness_et;

/*
 * inicjalizuje obsługę podświetlenia wskaźników
 */
void drv_backlight_init(void);

/*
 * ustawia jasność podświetlenia wskaźników
 *
 * @brightness	poziom podświetlenia
 */
void drv_backlight_set(brightness_et brightness);

