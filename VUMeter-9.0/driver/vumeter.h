#pragma once

/** \file **********************************************************************
 *
 * sterowanie miernikiem vu
 *
 ******************************************************************************/

/*
 * inicjalizacja miernika vu
 */
void drv_vumeter_init(void);

/*
 * włącza miernik vu
 */
void drv_vumeter_on(void);

/*
 * wyłącza miernik vu
 */
void drv_vumeter_off(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_vumeter_on_tick_time(void);

