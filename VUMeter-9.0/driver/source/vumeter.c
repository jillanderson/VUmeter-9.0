#include "../vumeter.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../hal/measurer.h"
#include "../../hal/source.h"

static struct {
	volatile uint8_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
} vumeter = { 0 };

static bool vumeter_is_on(void);

void drv_vumeter_init(void)
{
	hal_source_init();
	hal_measurer_init();
}

void drv_vumeter_on(void)
{
	/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
	static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

	if ((false == vumeter_is_on()) && (COUNTER_STOPPED == vumeter.counter_state)) {
		hal_source_on();
		vumeter.time_counter = RELAY_BOUNCE_TIME_MS;
		vumeter.counter_state = COUNTER_COUNTING;
	}
}

void drv_vumeter_off(void)
{
	hal_source_off();
	hal_measurer_off();
	vumeter.counter_state = COUNTER_STOPPED;
}

void drv_vumeter_on_tick_time(void)
{
	if (COUNTER_COUNTING == vumeter.counter_state) {

		if (0U < vumeter.time_counter) {
			vumeter.time_counter--;
		}

		if (0U == vumeter.time_counter) {
			hal_measurer_on();
			vumeter.counter_state = COUNTER_STOPPED;
		}
	}
}

bool vumeter_is_on(void)
{
	return (((true == hal_source_is_on()) && (true == hal_measurer_is_on())) ? true : false);
}

