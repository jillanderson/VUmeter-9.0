#pragma once

/** \file **********************************************************************
 *
 * sterowanie działaniem urządzenia
 *
 ******************************************************************************/

/*
 * inicjuje obsługę urządzenia
 */
void device_init(void);

/*
 * przetwarza zdarzenia
 * musi być wywoływana w nieskończonej pętli funkcji main
 */
void device_handle_events(void);

