#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikiem podłączającym miernik do sygnału
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje miernik sygnału
 */
void hal_measurer_init(void);

/*
 * przyłącza miernik do sygnału mierzonego
 */
void hal_measurer_on(void);

/*
 * odłącza miernik od sygnału mierzonego
 */
void hal_measurer_off(void);

/*
 * sprawdza czy miernik jest przyłączony do sygnału mierzonego
 *
 * @ret		true - przyłączony
 *		false - nie przyłączony
 */
bool hal_measurer_is_on(void);

