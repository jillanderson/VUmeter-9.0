#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikami podłączającymi sygnał do wskaźnika
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie źródłem sygnału
 */
void hal_source_init(void);

/*
 * podłącza źródło sygnału
 */
void hal_source_on(void);

/*
 * odłącza źródło sygnału
 */
void hal_source_off(void);

/*
 * sprawdza czy źródło sygnału jest podłączone
 *
 * @ret		true - podłączone
 * 		false - nie podłączone
 */
bool hal_source_is_on(void);

