#include "../backlight.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const LED_BACKLIGHT_LEFT = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC3 };
static avr_gpio_pin_st const LED_BACKLIGHT_RIGHT = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA0 };

static bool backlight_left_is_on(void);
static bool backlight_right_is_on(void);

void hal_backlight_init(void)
{
	avr_gpio_configure_as_output(&LED_BACKLIGHT_LEFT);
	avr_gpio_configure_as_output(&LED_BACKLIGHT_RIGHT);
	avr_gpio_set_low(&LED_BACKLIGHT_LEFT);
	avr_gpio_set_low(&LED_BACKLIGHT_RIGHT);
}

void hal_backlight_on(void)
{
	avr_gpio_set_high(&LED_BACKLIGHT_LEFT);
	avr_gpio_set_high(&LED_BACKLIGHT_RIGHT);
}

void hal_backlight_off(void)
{
	avr_gpio_set_low(&LED_BACKLIGHT_LEFT);
	avr_gpio_set_low(&LED_BACKLIGHT_RIGHT);
}

bool hal_backlight_is_on(void)
{
	return (((true == backlight_left_is_on())
			&& (true == backlight_right_is_on())) ? true : false);
}

bool backlight_left_is_on(void)
{
	return (0U == avr_gpio_read_pin(&LED_BACKLIGHT_LEFT) ? false : true);
}

bool backlight_right_is_on(void)
{
	return (0U == avr_gpio_read_pin(&LED_BACKLIGHT_RIGHT) ? false : true);
}

