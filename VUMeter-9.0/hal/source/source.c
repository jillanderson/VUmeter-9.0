#include "../source.h"

#include <avr/io.h>
#include <avr/avr_gpio.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_SOURCE_0 = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA4 };
static avr_gpio_pin_st const RELAY_SOURCE_1 = (avr_gpio_pin_st ) { .port = &PORTA, .pin_number = PA5 };

static bool source_0_is_on(void);
static bool source_1_is_on(void);

void hal_source_init(void)
{
	avr_gpio_configure_as_output(&RELAY_SOURCE_0);
	avr_gpio_configure_as_output(&RELAY_SOURCE_1);
	avr_gpio_set_low(&RELAY_SOURCE_0);
	avr_gpio_set_low(&RELAY_SOURCE_1);
}

void hal_source_on(void)
{
	avr_gpio_set_high(&RELAY_SOURCE_0);
	avr_gpio_set_high(&RELAY_SOURCE_1);
}

void hal_source_off(void)
{
	avr_gpio_set_low(&RELAY_SOURCE_0);
	avr_gpio_set_low(&RELAY_SOURCE_1);
}

bool hal_source_is_on(void)
{
	return (((true == source_0_is_on())
			&& (true == source_1_is_on())) ? true : false);
}

bool source_0_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_SOURCE_0) ? false : true);
}

bool source_1_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_SOURCE_1) ? false : true);
}

