#include "../measurer.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_MUTE = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC7 };

void hal_measurer_init(void)
{
	avr_gpio_configure_as_output(&RELAY_MUTE);
	avr_gpio_set_low(&RELAY_MUTE);
}

void hal_measurer_on(void)
{
	avr_gpio_set_high(&RELAY_MUTE);
}

void hal_measurer_off(void)
{
	avr_gpio_set_low(&RELAY_MUTE);
}

bool hal_measurer_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_MUTE) ? false : true);
}

