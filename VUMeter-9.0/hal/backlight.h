#pragma once

/** \file **********************************************************************
 *
 * sterowanie podświetleniem wskaźników VU
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje podświetlenie wskaźników wychyłowych
 */
void hal_backlight_init(void);

/*
 * włącza podświetlenie wskaźników wychyłowych
 */
void hal_backlight_on(void);

/*
 * wyłącza podświetlenie wskaźników wychyłowych
 */
void hal_backlight_off(void);

/*
 * sprawdza czy podświetlenie wskaźników wychyłowych jest włączone
 *
 * @ret		true - włączone
 *		false - wyłączone
 */
bool hal_backlight_is_on(void);

